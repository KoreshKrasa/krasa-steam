use rand::Rng;
use std::{io::stdin, path::Path};
use steam_roller::steam::SteamApp;
use steam_roller::SuperError;

fn main() -> Result<(), SuperError> {
    let mut rng = rand::thread_rng();
    let mut user_input = String::new();

    let steam_path =
        Path::new("/var/home/krasa/.var/app/com.valvesoftware.Steam/.local/share/Steam");
    let apps = SteamApp::load(&steam_path);
    let apps_count = apps.len();

    loop {
        let app = &apps[rng.gen_range(0..apps_count)];
        println!("Launch {} [y/N]", app.name().unwrap());
        stdin().read_line(&mut user_input).unwrap();
        if user_input.trim() == "y" {
            app.run();
            break;
        }
        user_input.clear();
    }
    Ok(())
}
