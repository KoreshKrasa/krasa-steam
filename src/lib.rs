use std::{
    collections::BTreeMap,
    error,
    fs::{read_dir, read_to_string},
    path::{Path, PathBuf},
};

pub type SuperError = Box<dyn error::Error>;

pub mod steam {
    use super::*;

    pub struct SteamApp {
        info: vdf::Vdf,
    }

    impl SteamApp {
        fn new(info: vdf::Vdf) -> Self {
            Self { info }
        }

        pub fn name(&self) -> Option<&str> {
            self.info
                .get("AppState")?
                .nested
                .as_ref()?
                .get("name")?
                .value
                .as_deref()
        }

        pub fn appid(&self) -> Option<&str> {
            self.info
                .get("AppState")?
                .nested
                .as_ref()?
                .get("appid")?
                .value
                .as_deref()
        }

        pub fn load(steam_path: &Path) -> Vec<Self> {
            let mut result: Vec<Self> = Vec::new();
            let mut steamapps_path = PathBuf::from(steam_path);
            steamapps_path.push("steamapps");

            for entry in read_dir(steamapps_path).unwrap() {
                let path = entry.unwrap().path();
                if path.is_file() {
                    if path.extension().unwrap() == "acf" {
                        result.push(Self::new(vdf::Vdf::parse(&path).unwrap()))
                    }
                }
            }
            result
        }

        #[cfg(target_os = "linux")]
        pub fn run(&self) {
            use ashpd::{desktop::open_uri::open_uri, WindowIdentifier};
            use futures::executor::block_on;

            let steam_url = format!("steam://run/{}", self.appid().unwrap());
            block_on(open_uri(&WindowIdentifier::None, &steam_url, false, false))
                .expect("Panic at the Distro?");
        }
    }

    mod vdf {
        use super::*;

        #[derive(Debug, Clone)]
        pub struct Vdf(VdfNode);

        impl Vdf {
            pub fn new() -> Self {
                Vdf(VdfNode::new())
            }

            pub fn get(&self, key: &str) -> Option<&VdfValue> {
                self.0.get(key)
            }

            pub fn get_mut(&mut self, key: &str) -> Option<&mut VdfValue> {
                self.0.get_mut(key)
            }

            pub fn parse(path: &Path) -> Result<Self, SuperError> {
                let mut vdf_obj = Vdf::new();
                let content = read_to_string(path)?;
                vdf_obj.0.append(&mut Self::_parse_vdf(&content)?);

                Ok(vdf_obj)
            }

            fn _parse_vdf(content: &str) -> Result<VdfNode, Box<dyn error::Error>> {
                let mut result: VdfNode = VdfNode::new();
                let mut line_counter = 0;
                let lines: Vec<&str> = content.lines().collect();
                let line_count = content.lines().count();
                while line_counter < line_count {
                    let line = lines[line_counter];

                    if line.is_empty() || line.contains("{") || line.contains("}") {
                        line_counter += 1;
                        continue;
                    }

                    if line.contains("\"\t\t\"") {
                        let deelimiter_pos: Vec<_> = line.match_indices("\"").collect();
                        let key = &line[deelimiter_pos[0].0 + 1..deelimiter_pos[1].0];
                        let value = &line[deelimiter_pos[2].0 + 1..deelimiter_pos[3].0];

                        let mut vdf_content = VdfNode::new();
                        vdf_content.insert(key.to_string(), VdfValue::new(Some(value), None));
                        result.append(&mut vdf_content);
                        line_counter += 1;
                        continue;
                    } else {
                        let deelimiter_pos: Vec<_> = content.match_indices("\"").collect();
                        let mut nester_len = 0;
                        let nested_key = &line[deelimiter_pos[0].0 + 1..deelimiter_pos[1].0];
                        let next_line = lines[line_counter + 1];
                        while lines[line_counter + nester_len] != next_line.replace("{", "}") {
                            nester_len += 1;
                        }

                        let sub_content =
                            lines[line_counter + 2..line_counter + nester_len].join("\n");
                        let sub_result = Self::_parse_vdf(&sub_content)?;
                        let mut vdf_content = VdfNode::new();
                        vdf_content.insert(
                            nested_key.to_string(),
                            VdfValue::new(None, Some(sub_result)),
                        );
                        result.append(&mut vdf_content);
                        line_counter += nester_len + 2;
                        continue;
                    }
                }
                Ok(result)
            }
        }

        type VdfNode = BTreeMap<String, VdfValue>;
        #[derive(Debug, Clone)]
        pub struct VdfValue {
            pub value: Option<String>,
            pub nested: Option<VdfNode>,
        }

        impl VdfValue {
            pub fn new(value: Option<&str>, nested: Option<VdfNode>) -> Self {
                Self {
                    value: match value {
                        Some(val) => Some(val.to_string()),
                        None => None,
                    },
                    nested,
                }
            }
        }
    }
}
